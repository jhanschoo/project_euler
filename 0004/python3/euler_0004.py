import math

"""
the product of two 3-digit numbers is bounded above by
1_000_000
Next p must satisfy
100_000x = x, 10_000y = 10y, 1_000z = 100z, where (x,y,z) are the
three palindromic digits of p. Hence
p = 100_001x + 10_001y + 1_100z
p = 11(9_091x + 910y + 100z)
So we see that if p = a * b, then one of a or b must be a
multiple of 11.
"""

def is_palindrome(n):
    if (n % 10 == n // 100_000
        and (n // 10) % 10 == (n // 10_000) % 10
        and (n // 100) % 10 == (n // 1_000) % 10):
        return True
    return False

if __name__ == "__main__":
    palindrome = 0
    for a in range(999, 99, -1):
        if a % 11 == 0:
            for b in range(999, a-1, -1):
                if a * b <= palindrome:
                    break
                if is_palindrome(a * b):
                    palindrome = a * b
        else:
            for b in range(990, a-1, -11):
                if a * b <= palindrome:
                    break
                if is_palindrome(a * b):
                    palindrome = a * b
    print(palindrome)