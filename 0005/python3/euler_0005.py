import math

"""
TODO: implement the more sophisticated algorithm in the overview
"""

MAX_DIVISOR = 20

def factorize(n):
    factors = {}
    i = 2
    while n != 1:
        while n % i == 0:
            n /= i
            if i not in factors:
                factors[i] = 0
            factors[i] += 1
        i += 1
    return factors

if __name__ == "__main__":
    factorization = {}
    for d in range (2, MAX_DIVISOR + 1):
        d_factorization = factorize(d)
        for k in d_factorization:
            if k not in factorization:
                factorization[k] = d_factorization[k]
            else:
                factorization[k] = max(factorization[k], d_factorization[k])
    n = 1
    for k, v in factorization.items():
        n *= k**v
    print(n)
